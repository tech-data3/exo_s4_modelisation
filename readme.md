# Modelisation formation tech-data

## Note

a regler:

- [ ] un apprenant peut etre dans 2 groupe pour le meme exo ....
- [ ] les dates de debut peuvent etre apres les dates de fin ???
- [ ] comment on calcul les jours de presence ?

## MCD

```mermaid
classDiagram
apprenant --> groupe: emprunte
groupe --> exercice: realise
exercice --> competence: utilise
diplome --> competence: valide
apprenant --> materiel: emprunte
absence <-- apprenant: pointe
```

## MLD

```
erDiagram

apprenant{
    apprenant_id int PK 
    nom varchar(50) "NOT NULL"
    prenom varchar(50) "NOT NULL"
    mail varchar(50) PK "NOT NULL"
    date_debut date "NOT NULL"
    date_fin date "NOT NULL"
}

exercice{
    exercice_id int PK
    nom varchar(50) "NOT NULL"
    date_debut date "NOT NULL"
    date_fin date "NOT NULL"
}

competence{
    competence_id int PK
    libele varchar(50) PK "NOT NULL"
    diplome_id int
}
group_apprenant{
    apprenant_id int FK,PK
    group_id int PK
}

group_comp_exo{
    group_id int FK
    competence_id int FK
    exercice_id int FK
    valide bool 
}

diplome{
    diplome_id int PK
    libele varchar(50) "NOT NULL"
}

diplome_apprenant{
    diplome_id int PK
    apprenant_id int PK, FK
    valide bool
    date_de_passage date
}

materiel{
    materiel_id int PK
    appprenant_id int FK "NOT NULL"
    libele varchar(50) "NOT NULL"
}

absence_apprenant{
    apprenant_id int PK, FK
    date_debut date PK "NOT NULL"
    date_fin date "NOT NULL"
}

apprenant ||--o{ group_apprenant: ""
group_apprenant }o--|| group_comp_exo: ""
group_comp_exo }o--|| exercice : ""
competence }|--o| diplome: ""
group_comp_exo }o--|| competence : ""
diplome |o--|{ diplome_apprenant : ""
diplome_apprenant }o--|| apprenant : ""
absence_apprenant }o--|| apprenant: ""
materiel }o--o| apprenant: ""
```

### dependences fonctionnelles

#### Dans la table apprenant

On a:

$$mail\rightarrow \begin{cases}
               nom\\
               prenom
            \end{cases}$$

#### Dans la table competence

On a:

$$libele\rightarrow diplome\_id$$

## MPD

![modele physique des donnees](init.sql)

### Set de donnees de test

![fichier pour remplir la bdd](test_data.sql)

## Questions

### liste des apprenants

#### Requete
```sql
SELECT * FROM apprenants
```

#### Reponse

| apprenant_id | nom       | prenom        | mail                            | date_debut | date_fin   |
|-------------:|:----------|:--------------|:--------------------------------|:-----------|:-----------|
|            1 | Opalina   | Mugglestone   | omugglestone0@columbia.edu      | 2024-01-13 | 2023-07-03 |
|            2 | Roselin   | Devall        | rdevall1@google.com             | 2023-06-21 | 2023-02-02 |
|            3 | Jesus     | Storie        | jstorie2@bloomberg.com          | 2023-07-05 | 2023-03-09 |
|            4 | Kacy      | Fanthom       | kfanthom3@ucoz.com              | 2023-09-15 | 2024-01-06 |
|            5 | Sutherlan | Andrews       | sandrews4@people.com.cn         | 2023-06-12 | 2023-07-05 |
|            6 | Wilbert   | Sugg          | wsugg5@soundcloud.com           | 2024-01-02 | 2024-01-02 |
|            7 | Gayla     | Mundall       | gmundall6@friendfeed.com        | 2023-10-25 | 2023-02-14 |
|            8 | Thalia    | Jorgesen      | tjorgesen7@wp.com               | 2023-02-05 | 2023-04-04 |
|            9 | Joyce     | Willeson      | jwilleson8@washingtonpost.com   | 2023-04-17 | 2023-08-04 |
|           10 | Almire    | Reeks         | areeks9@xrea.com                | 2023-06-09 | 2023-07-22 |
|           11 | Cthrine   | Francescuccio | cfrancescuccioa@kickstarter.com | 2023-09-16 | 2023-03-20 |
|           12 | Patrizia  | Andrichak     | pandrichakb@addtoany.com        | 2023-04-22 | 2023-02-05 |
|           13 | Kasey     | Holyland      | kholylandc@va.gov               | 2023-08-01 | 2023-06-30 |
|           14 | Dru       | Longo         | dlongod@skype.com               | 2023-03-31 | 2023-09-12 |
|           15 | Jolee     | Volet         | jvolete@soundcloud.com          | 2023-04-03 | 2023-03-06 |
|           16 | Delano    | Vine          | dvinef@unblog.fr                | 2024-01-07 | 2023-07-09 |
|           17 | Syd       | Ivashov       | sivashovg@desdev.cn             | 2023-09-29 | 2024-01-21 |
|           18 | Ailene    | Reddlesden    | areddlesdenh@si.edu             | 2023-04-09 | 2023-02-11 |
|           19 | Alysia    | Meller        | amelleri@google.it              | 2023-03-28 | 2023-06-15 |
|           20 | Tab       | Cecere        | tcecerej@globo.com              | 2023-06-15 | 2023-09-10 |

### nombre de compétence validée par apprenant

#### Requete

```sql
select a.apprenant_id, count(c.competence_id) AS `nb competences validees`
from apprenant as a 
join groupe_apprenant as ga on ga.apprenant_id = a.apprenant_id 
join group_comp_exo as gce on gce.group_id = ga.group_id and gce.valide = True 
join competence c on c.competence_id = gce.competence_id 
group by a.apprenant_id;
```

#### Reponse

| apprenant_id | nb competences validees|
|:-------------|-----------------------:|
|           20 |                      3 |
|            7 |                      2 |
|            9 |                      4 |
|           15 |                      6 |
|           13 |                      2 |
|           17 |                      1 |
|           18 |                      1 |
|           19 |                      1 |
|            1 |                      1 |
|            8 |                      1 |
|           12 |                      1 |
|           14 |                      1 |
|            3 |                      1 |
|           10 |                      4 |
|           11 |                      4 |
|           16 |                      1 |

### nombre d'absence (jours) par apprenants

#### Requete

#### Reponse

### nombre d'absence (jours) et nombre de compétence validée

#### Requete

#### Reponse

### tout les apprenants d'un groupe

#### Requete

```sql
SELECT * FROM groupe_apprenant
WHERE group_id = 2
```

#### Reponse

| group_id | apprenant_id |
|---------:|-------------:|
|        2 |            7 |
|        2 |            9 |
|        2 |           15 |

### nombre de groupe auxquels chaque apprenant a participé

#### Requete

```sql
SELECT a.apprenant_id, count(distinct ga.group_id) as `nb groupe ditinct`
FROM groupe_apprenant as ga 
JOIN apprenant AS a on ga.apprenant_id = a.apprenant_id 
group by a.apprenant_id
ORDER BY a.apprenant_id;

```
#### Reponse

| apprenant_id | nb groupe ditinct           |
|-------------:|----------------------------:|
|            1 |                           1 |
|            3 |                           1 |
|            7 |                           2 |
|            8 |                           1 |
|            9 |                           4 |
|           10 |                           2 |
|           11 |                           2 |
|           12 |                           1 |
|           13 |                           2 |
|           14 |                           2 |
|           15 |                           4 |
|           16 |                           1 |
|           17 |                           1 |
|           18 |                           1 |
|           19 |                           1 |
|           20 |                           3 |


### pour 1 apprenant (avec son nom) la liste des compétences et leur état (validé ou pas)

#### Requete

```sql
select a.apprenant_id, c.libele, gce.valide
from apprenant as a 
join groupe_apprenant as ga on ga.apprenant_id = a.apprenant_id 
join group_comp_exo as gce on gce.group_id = ga.group_id  
join competence c on c.competence_id = gce.competence_id 
where a.nom = "Alysia";
```
#### Reponse

| apprenant_id | libele                | valide |
|-------------:|:----------------------|-------:|
|           19 | amet turpis elementum |      1 |
|           19 | augue a suscipit      |      0 |

### le plus absent qui doit ramener des pains au chocolat.

#### Requete

#### Reponse

### pour une date donnée la liste des présents.

#### Requete

#### Reponse

### liste des apprenants avec leur oridnateur / matériel

#### Requete

#### Reponse

### pour un projet la liste des apprenant et leur groupe

#### Requete

#### Reponse

