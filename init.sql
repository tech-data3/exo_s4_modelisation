DROP DATABASE IF EXISTS formation;

CREATE DATABASE formation;

USE formation;

CREATE TABLE apprenant(
    apprenant_id INT PRIMARY KEY AUTO_INCREMENT, 
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    mail VARCHAR(50) UNIQUE NOT NULL,
    date_debut DATE NOT NULL,
    date_fin DATE NOT NULL
);

CREATE TABLE groupe_apprenant(
    group_id INT,
    apprenant_id INT REFERENCES apprenant.apprenant_id,
    PRIMARY KEY(group_id, apprenant_id)
);

CREATE TABLE exercice(
    exercice_id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    date_debut DATE NOT NULL,
    date_fin DATE NOT NULL
);

CREATE TABLE group_comp_exo(
    group_id INT REFERENCES group_apprenant.group_id,
    competence_id INT REFERENCES competence.competence_id,
    exercice_id INT REFERENCES exercice.exercice_id,
    valide bool,
    UNIQUE (competence_id, exercice_id),
    UNIQUE (exercice_id, group_id),
    UNIQUE (competence_id, exercice_id, group_id)
);

CREATE TABLE diplome(
    diplome_id INT PRIMARY KEY AUTO_INCREMENT,
    libele VARCHAR(50) NOT NULL
);

CREATE TABLE competence(
    competence_id INT PRIMARY KEY,
    libele VARCHAR(50) NOT NULL,
    diplome_id INT REFERENCES diplome.diplome_id
);

CREATE TABLE diplome_apprenant(
    diplome_id INT REFERENCES diplome.diplome_id,
    apprenant_id INT REFERENCES apprenant.apprenant_id,
    valide BOOL,
    date_de_passage DATE,
    PRIMARY KEY(diplome_id, apprenant_id)
);

CREATE TABLE materiel(
    materiel_id INT PRIMARY KEY AUTO_INCREMENT,
    apprenant_id INT REFERENCES apprenant.apprenant_id,
    libele VARCHAR(50) NOT NULL
);

CREATE TABLE absence_apprenant(
    apprenant_id INT REFERENCES apprenant.apprenant_id,
    date_debut DATE NOT NULL,
    date_fin DATE NOT NULL,
    PRIMARY KEY(apprenant_id, date_debut)
);

